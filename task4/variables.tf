variable "windows_ami" {
  type = string
}

variable "linux_ami" {
  type = string
}

variable "instance_type" {
  type = string
}

variable "key_pair" {
  type = string
}
