#!/bin/bash
#
# Script to create RDP security groups

RDP_SG_NAME=allow_rdp

#source ../../get_default_vpc.sh # return VpcID on DEFAULT_VPC_ID variable

# Creating SSH security group
rdp_sg=$(aws ec2 describe-security-groups \
          --group-names $RDP_SG_NAME \
          --query "SecurityGroups[*].GroupId" \
          --output text 2> /dev/null)

if [[ -z $rdp_sg ]]; then
  echo "Creating RDP security group"
  rdp_sg=$(aws ec2 create-security-group \
        --description 'RDP Security Group' \
        --group-name $RDP_SG_NAME \
        --vpc-id $DEFAULT_VPC_ID  \
        --tag-specifications "ResourceType=security-group,Tags=[{Key=Name, Value=$RDP_SG_NAME}]" \
        --query "GroupId" --output text)
fi

echo "RDP security group: $rdp_sg"

# Adding ingress to RDP security group
echo "Adding 3389/tcp from 0.0.0.0/0 ingress to RDP security group"
aws ec2 authorize-security-group-ingress \
    --group-id $rdp_sg \
    --protocol tcp \
    --port 3389 \
    --cidr 0.0.0.0/0

