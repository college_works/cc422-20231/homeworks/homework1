#!/bin/bash
#
# Script to create SSH security groups

#SSH_SG_NAME=allow_ssh # (defined on $ROOT/vars.sh)

# source ../../get_default_vpc.sh # return VpcID on DEFAULT_VPC_ID variable (defined on $ROOT/vars.sh)

# Creating SSH security group
ssh_sg=$(aws ec2 describe-security-groups \
          --group-names $SSH_SG_NAME \
          --query "SecurityGroups[*].GroupId" \
          --output text 2> /dev/null)

if [[ -z $ssh_sg ]]; then
  echo "Creating SSH security group"
  ssh_sg=$(aws ec2 create-security-group \
        --description 'SSH Security Group' \
        --group-name $SSH_SG_NAME \
        --vpc-id $DEFAULT_VPC_ID  \
        --tag-specifications "ResourceType=security-group,Tags=[{Key=Name, Value=$SSH_SG_NAME}]" \
        --query "GroupId" --output text)
fi

echo "SSH security group: $ssh_sg"

# Adding ingress to SSH security group
echo "Adding 22/tcp from 0.0.0.0/0 ingress to SSH security group"
aws ec2 authorize-security-group-ingress \
    --group-id $ssh_sg \
    --protocol tcp \
    --port 22 \
    --cidr 0.0.0.0/0

