#!/bin/bash
#
# Call script on security_groups to create defined security groups

source vars.sh

source pretasks/security_groups/ssh.sh
source pretasks/security_groups/rdp.sh
