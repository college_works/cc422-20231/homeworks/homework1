#!/bin/bash
#
# Create pair keys
#
# NOTE: AWS is completly responsible for generating your cryptographic material
#
# If you are want to be responsable of the creation of it use the following commands for that:
# - [Generation of key] 
#   ssh-keygen -m PEM -q -b $KEY_BITES -t $KEY_TYPE -f $KEY_NAME.pem
#
# - [Import key] 
#   aws ec2 import-key-pair \
#     --key-name $KEY_NAME \
#     --region $REGION \
#     --public-key-material fileb://$PWD/$KEY_NAME.pem.pub


source vars.sh

# Verifing if KEYNAME key exist
key_pair=$(aws ec2 describe-key-pairs \
            --key-name $KEY_NAME \
            --query 'KeyPairs[].KeyName' \
            --output text)

if [[ -z $key_pair ]]; then
  echo "Creating $KEYNAME key pair"

  aws ec2 create-key-pair \
    --key-name $KEY_NAME \
    --query 'KeyMaterial' \
    --key-format $KEY_FORMAT \
    --key-type $KEY_TYPE \
    --output text > $KEY_NAME.$KEY_FORMAT

  echo "Key-Pair Location: $PWD/$KEY_NAME.$KEY_FORMAT"
fi

