#!/bin/bash
#
# Get default vpc ID

DEFAULT_VPC_ID=$(aws ec2 describe-vpcs \
  --filter 'Name=is-default, Values=true' \
  --query 'Vpcs[*].VpcId' \
  --output text)

echo "Default vpc ID: $DEFAULT_VPC_ID"
