#!/bin/bash
#
# This bash script do task1 of homework1 
# of the course CC422.
#
# TASK 1:
# - Create an EC2 instance with Windows OS
#
# NOTE:
# - All resources are deployed on the default region and VPC


source vars.sh


# Windows Instance AMI
# WINDOWS_AMI='ami-0be0e902919675894' # (defined on vars.sh as WINDOWS_AMI)

echo "OS AMI: $WINDOWS_AMI"

# Getting id of RDP security group
rdp_sg=$(aws ec2 describe-security-groups \
      --filter "Name=group-name, Values=$RDP_SG_NAME" \
			--query 'SecurityGroups[].GroupId' \
      --output text)

# Selecting the first subnet of default VPC (just for simplicity)
subnetId=$(aws ec2 describe-subnets \
			--filter "Name=vpc-id,Values=$DEFAULT_VPC_ID" \
			--query "Subnets[0].SubnetId" \
      --output text)

echo "subnetId: $subnetId"
			

# Launch windows EC2 instance
echo "Lunching Micrsoft Windows EC2 instance"
instanceId=$(aws ec2 run-instances --image-id $WINDOWS_AMI \
              --instance-type $INSTANCE_TYPE \
              --key-name $KEY_NAME \
              --security-group-ids $rdp_sg \
              --subnet-id $subnetId \
              --query 'Instances[].InstanceId' \
              --output text)

echo "Instance information (InstanceId: $instanceId):"
aws ec2 wait instance-running --instance-ids $instanceId
aws ec2 describe-instances --instance-ids $instanceId \
    | grep -E "KeyName|PublicIpAddress|Platform"	
