#!/bin/bash
#
# Common variables

# key-pair
KEY_NAME=homework1 
KEY_FORMAT=pem
KEY_TYPE=rsa

# security groups
SSH_SG_NAME=allow_ssh
RDP_SG_NAME=allow_rdp

# vpc
source get_default_vpc.sh # define DEFAULT_VPC_ID

# EC2 instances (REGION: us-east-1)
INSTANCE_TYPE='t2.micro'

WINDOWS_AMI="ami-0be0e902919675894"
# old WINDOWS 'ami-0be0e902919675894'
LINUX_AMI="ami-03a6eaae9938c858c"
#"ami-06e46074ae430fba6"
